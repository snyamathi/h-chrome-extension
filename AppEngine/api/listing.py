import webapp2
import jinja2
import os
import json

from models import Listing

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class GetByNeweggIdHandler(webapp2.RequestHandler):
    def get(self):
        _newegg_item_id = self.request.get('newegg_item_id')
        if _newegg_item_id is None:
            self.error(400)
            json_response = {'listings' : []}
            self.response.out.write(json.dumps(json_response))
            return

        query = Listing.query(Listing.newegg_item_id == _newegg_item_id)
        listings = []
        for listing in query:
            listings.append(listing.to_dict())
        json_response = {'listings' : listings}
        self.response.out.write(json.dumps(json_response))
        
class AddHandler(webapp2.RequestHandler):
    def post(self):
        _hard_username = self.request.get('hard_username')
        _hard_url = self.request.get('hard_url')
        _price = self.request.get('price')
        _newegg_item_id = self.request.get('newegg_item_id')
        
        if _hard_username is None:
            self.error(400)
            return
        elif _hard_url is None:
            self.error(400)
            return
        elif _price is None:
            self.error(400)
            return
        elif _newegg_item_id is None:
            self.error(400)
            return

        listing = Listing(hard_username = _hard_username, hard_url = _hard_url, price = _price, newegg_item_id = _newegg_item_id)
        listing.put()
        json_response = {'result' : 'success'}
        self.response.out.write(json.dumps(json_response))
        
class DisplayByNeweggIdHandler(webapp2.RequestHandler):
    def get(self):        
        _newegg_item_id = self.request.get('newegg_item_id')
        if _newegg_item_id is None or len(_newegg_item_id) is 0:
            self.error(400)
            return
        
        query = Listing.query(Listing.newegg_item_id == _newegg_item_id)
        
        template_values = {
            'listings' : query
        }
        
        template = jinja_environment.get_template('response.html')
        self.response.out.write(template.render(template_values))        
        
app = webapp2.WSGIApplication([('/listing/add', AddHandler),
                               ('/listing/getByNeweggId', GetByNeweggIdHandler),
                               ('/listing/displayByNeweggId', DisplayByNeweggIdHandler)], debug=True)