from google.appengine.ext import ndb

class Listing(ndb.Model):
    hard_username = ndb.StringProperty()
    hard_url = ndb.StringProperty();
    price = ndb.StringProperty();
    newegg_item_id = ndb.StringProperty()
    
    @classmethod
    def property_names(cls):
        names = []
        
        for name in cls.__dict__:
            attribute = getattr(cls, name)
            if isinstance(attribute, ndb.Property):
                names.append(name)
                
        return names
    
    def to_dict(self):
        obj = {};
        for name in self.property_names():
            attr = getattr(self, name)
            if attr is not None:
                obj[name] = unicode(attr)

        return obj