chrome.browserAction.setBadgeText({text: "10+"});
chrome.tabs.getSelected(null, function(tab) {
  tabUrl = tab.url;

  var hostname = $('<a>').prop('href', tabUrl).prop('hostname');
  if (hostname == 'www.newegg.com') {
	var itemId = tabUrl.substring(tabUrl.indexOf('Item=')+5);
	displayListingsForNeweggId(itemId);
  }
	
});

function displayListingsForNeweggId(itemId) {
	$.get('http://www.hardforumchromextension.appspot.com/listing/displayByNeweggId?newegg_item_id=' + itemId, function(response) {			
			$('#content').html(response)
		}
	)	
}